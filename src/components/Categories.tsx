import React from "react";

const categories = [
    'Все',
    'Мясные',
    'Вегетарианская',
    'Гриль',
    'Острые',
    'Закрытые',
]

type CategoriesProps = {
    value: number;
    onChangeCategory: (index: number) => void;
}

const Categories:React.FC<CategoriesProps> = ({value, onChangeCategory}) => {

    return (
        <div className="categories">
            <ul>
                {
                    categories.map( (categoryName, index) => (
                            <li
                                key={index}
                                onClick={() => onChangeCategory(index)}
                                className={value === index ? "active": ""}
                            >{categoryName}</li>
                        )
                    )
                }
            </ul>
        </div>
    )
}

export default Categories;